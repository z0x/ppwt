#!/usr/bin/env bash
set -e

custom_mac_entered=0
param_mac=""
ref_site="gazette.com" ## any site that still doesn't use https

while getopts 'm:' flag; do
        case "${flag}" in
            m) custom_mac_entered=1 && param_mac=${OPTARG} ;;
        esac
done

aruba_capture_html=$( wget --max-redirect=0 --no-check-certificate --no-dns-cache -O - "$ref_site" )
aruba_capture_meta_refresh_tag_arr=$( awk '/meta/ { print $4 }' <<< "$aruba_capture_html" )
aruba_capture_meta_refresh_tag=""

for i in ${aruba_capture_meta_refresh_tag_arr[@]} ; do :
    if [ ! -z "$i" ] ; then
        aruba_capture_meta_refresh_tag="$i"
    fi
done

if [ -z "$aruba_capture_meta_refresh_tag" ] ; then
	echo "You are either already connected, or something's gone horribly wrong. Sometimes its hard to tell."
	exit 0
fi

aruba_capture_meta_url_dirty=$( awk 'BEGIN {FS="url="} { print $2 }' <<< "$aruba_capture_meta_refresh_tag" )
aruba_capture_meta_url=${aruba_capture_meta_url_dirty%?}

tos_url=$( curl -Ls -o /dev/null -w %{url_effective} "$aruba_capture_meta_url" )
tos_url_host=$( awk 'BEGIN {FS="?"} {print $1}' <<< "$tos_url" | tr -dc [0-9]. )
tos_url_host=${tos_url_host%?}
tos_url_param_string=$( awk 'BEGIN {FS="?"} {print $2}' <<< "$tos_url" )

param_ip=$( awk 'BEGIN {FS="&"} {print $3}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )
param_url=$( awk 'BEGIN {FS="&"} {print $7}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )

if [ "$custom_mac_entered" -eq 0 ] ; then
    param_mac=$( awk 'BEGIN {FS="&"} {print $2}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )
    curl 'http://'$tos_url_host'/auth/index.html/u' -H 'Host: '$tos_url_host'' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: '$tos_url'' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Cookie: CPsession='$param_url'ip%3D'$param_ip'' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' --data 'email=user%40ppld.org&cmd=authenticate&Login=I+ACCEPT' > /dev/null
else
    param_cmd=$( awk 'BEGIN {FS="&"} {print $1}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )
    param_essid=$( awk 'BEGIN {FS="&"} {print $4}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )
    param_apname=$( awk 'BEGIN {FS="&"} {print $5}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )
    param_apgroup=$( awk 'BEGIN {FS="&"} {print $6}' <<< "$tos_url_param_string" | awk 'BEGIN {FS="="} {print $2}' )

    split_ip_1=$( awk 'BEGIN {FS="."} {print $1}' )
    split_ip_2=$( awk 'BEGIN {FS="."} {print $2}' )
    split_ip_3=$( awk 'BEGIN {FS="."} {print $3}' )
    split_ip_4=$( awk 'BEGIN {FS="."} {print $4}' )
    let "split_ip_4+=1"
    param_ip="$split_ip_1.$split_ip_2.$split_ip_3.$split_ip_3.$split_ip_4"
    custom_mac_url="http://$tos_url_host/upload/custom/PPLDW-cp_prof/ppldwhite.html?cmd=login&mac=$param_mac&ip=$param_ip&essid=$param_essid&apname=$param_apname&apgroup=$param_apgroup&url=$param_url"
    wget --no-check-certificate -O - "$custom_mac_url" > /dev/null
    curl 'http://'$tos_url_host'/auth/index.html/u' -H 'Host: '$tos_url_host'' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: '$custom_mac_url'' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Cookie: CPsession='$param_url'ip%3D'$param_ip'' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' --data 'email=user%40ppld.org&cmd=authenticate&Login=I+ACCEPT' > /dev/null
fi