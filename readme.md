# ppld_wifiauth
Agree to Pike's Peak Library District's captive portal TOS from the command line.

#### Usage

`./ppld_wifiauth.sh` - send a series of http requests that authenticates  the current machine

`./ppld_wifiauth.sh -m "00:00:00:00:00:00"` - Authenticate a device by mac address. Good for devices that don't play nice with captive portals.